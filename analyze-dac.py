#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

plt.style.use('ggplot')
output_dir = '.'


def transient(data):
    fig, ax = plt.subplots(tight_layout=True)
    ax.plot(1e9*data[:, 0], 1e6*data[:, 1], 'r-', label='$I_{op}$', alpha=0.7)
    ax.plot(1e9*data[:, 2], 1e6*data[:, 3], 'b-', label='$I_{om}$', alpha=0.7)
    ax.set(xlabel='time (ns)', ylabel='current ($\mu$A)')
    ax.legend()
    plt.savefig(output_dir + '/dac_transient.png', transparent=False)


def error(data, num_bits=4, inl=True, dnl=True):
    length = 2**num_bits
    step_size = data.shape[0] // length
    init_point = 98 * step_size // 100
    sample_points = range(init_point, data.shape[0], step_size)
    sampled_data = data[sample_points, :]

    lsb = np.sum(sampled_data) / (length * (length-1))
    print(f'One LSB Current is: {lsb}')

    sampled_data_lsb = sampled_data
    sampled_data_lsb[:, [1, 3]] /= lsb

    input_range = range(0, length, 1)
    x = [bin(byte)[2:].zfill(num_bits) for byte in input_range]

    if dnl:
        dnl = np.diff(sampled_data_lsb[:, [1, 3]], axis=0)
        dnl[:, 0] -= 1
        dnl[:, 1] += 1
        fig, ax = plt.subplots(tight_layout=True)
        ax.set_xticklabels(x, rotation=-60)
        ax.plot(x[1:], dnl[:, 0], 'r-', label='DNL: $I_{op}$')
        ax.plot(x[1:], dnl[:, 1], 'b-', label='DNL: $I_{om}$')
        ax.set(xlabel='Digital Input', ylabel='DNL Error (LSB)')
        ax.legend()
        plt.savefig(output_dir + '/dac_dnl.png', transparent=False)
        print(f'Max DNL: {np.max(np.abs(dnl))}')

    if inl:
        inl = sampled_data_lsb[:, [1, 3]] - np.transpose([input_range,
                                                          input_range[::-1]])
        fig, ax = plt.subplots(tight_layout=True)
        ax.set_xticklabels(x, rotation=-60)
        ax.plot(x, inl[:, 0], 'r-', label='INL: $I_{op}$')
        ax.plot(x, inl[:, 1], 'b-', label='INL: $I_{om}$')
        ax.set(xlabel='Digital Input', ylabel='INL Error (LSB)')
        ax.legend()
        plt.savefig(output_dir + '/dac_inl.png', transparent=False)
        offset = sampled_data_lsb[0, [1, 3]]
        print(f'Offsets (percent of LSB): {100*offset}')
        print(f'Max INL: {np.max(np.abs(inl))}')


def clean():
    import subprocess
    for filename in ['dac_inl.png', 'dac_dnl.png', 'dac_transient.png']:
        subprocess.run(['rm', filename])


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Analyze an N-bit DAC given a'
                                     ' one period data file from SPICE')

    parser.add_argument('filename', help='The name of the file produced by '
                        'SPICE wrdata command that contains the transient '
                        '\'staircase\' data', nargs='?', default=None)
    parser.add_argument('-d', '--dnl', action='store_true', dest='dnl',
                        help='create DNL analysis')
    parser.add_argument('-i', '--inl', action='store_true', dest='inl',
                        help='create INL analysis')
    parser.add_argument('-t', '--transient', action='store_true',
                        dest='transient',
                        help='create transient staircase analysis')
    parser.add_argument('-s', '--show', action='store_true', dest='show',
                        help='show graph(s)')
    parser.add_argument('-n', '--num_bits', dest='num_bits', default=4,
                        type=int, help='resolution of the DAC (default=4)')
    parser.add_argument('-c', '--clean', action='store_true', dest='clean',
                        help='cleans .png output files (use this option by '
                        'itself)')

    args = parser.parse_args()

    if args.clean:
        clean()
    else:
        if args.filename is None:
            raise Exception('Need filename!')
        data = np.genfromtxt(args.filename)

        if args.transient:
            transient(data)
        if args.inl or args.dnl:
            error(data, num_bits=args.num_bits, inl=args.inl,
                  dnl=args.dnl)
        if args.show:
            plt.show()
