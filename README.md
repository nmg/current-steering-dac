# Description

Testbench and analyze a current-steering 4-bit DAC. 

# Usage

First generate the testbench.data file from SPICE. 

    ngspice testbench.cir

Then use the analyze-dac.py command as follows:

    usage: analyze-dac.py [-h] [-n NUM_BITS] [-d] [-i] [-t] [-s] [-c] [filename]
    
    Analyze an N-bit DAC given a one period data file from SPICE
    
    positional arguments:
      filename              The name of the file produced by SPICE wrdata command
                            that contains the transient 'staircase' data
    
    optional arguments:
      -h, --help            show this help message and exit
      -n NUM_BITS, --num_bits NUM_BITS
                            resolution of the DAC
      -d, --dnl             create DNL analysis
      -i, --inl             create INL analysis
      -t, --transient       create transient staircase analysis
      -s, --show            show graph(s)
      -c, --clean           cleans .png output files (use this option by itself)

## Examples

Generate the transient data plots:

    python3 analyze-dac.py -t testbench.data

Generate transient, INL, and DNL data plots:

    python3 analyze-dac.py -dit testbench.data

Generate transient, INL, and DNL data plots, and show them:

    python3 analyze-dac.py -dits testbench.data

Clean up all those .png files:

    python3 analyze-dac.py -c

# Requirements

### git submodules

This repository leverages git submodules to include a different repository of
components already netlisted in spice. To include these necessary files, either
clone this repository with the '''--recurse-submodules''' option, or, enter
'''git submodule init && git submodule update''' after cloning without the
option. 

### ngspice, version >= 27

**Via Ubuntu:**

    sudo apt install ngspice

**Via any other way:**

see http://ngspice.sourceforge.net/download.html

**Note:**

Other SPICE simulators can be used, however are untested. ngspice has been set 
with spice3 compatibility mode, therefore other spice3
compatible simulators should work. 

### Python, version >=3.6

### Python packages matplotlib, numpy

    pip install --user numpy matplotlib

# Contributions

I am open to suggestions. Merge requests and emails are welcome. 
